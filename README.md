# OpenML dataset: click_prediction_small

https://www.openml.org/d/43901

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Modified version of subsampled dataset from Tencent Inc. on OpenML. Duplicate rows are dropped. Columns with a high ratio of unique values are dropped. Some columns are cast to factor.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43901) of an [OpenML dataset](https://www.openml.org/d/43901). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43901/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43901/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43901/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

